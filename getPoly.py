import cv2 as cv
import numpy as np
import argparse, sys
import random as rng
rng.seed(12345)

def parseArgs(argv=sys.argv[1:]):
    parser = argparse.ArgumentParser(description='get polygon')
    parser.add_argument('--image','-i', help='imagen para analizar', type=str, default='bordes.png')
    return parser.parse_args(sys.argv[1:])

def quickShow(img):#, z=1):
    '''Show image, press any key to close
    '''
    cv.namedWindow('tmp',cv.WINDOW_FREERATIO)
    #H,W = img.shape
    #reimg = cv.resize(img, (H*z,W*z))
    #cv.imshow('tmp',reimg)
    cv.imshow('tmp',img)
    print('\tresize with mouse, press any key to close...')
    cv.waitKey(0)
    cv.destroyWindow('tmp')

def getColors(img):
    H,W = img.shape
    colors=[]
    for i in range(W):
        for j in range(H):
            if not img[j,i] in colors:
                colors+=[img[j,i]]
    colors.sort()
    return colors, colors[0], colors[-1]

def getHistogram(img, minColor, maxColor, nbins=7):
    bins = np.linspace(minColor, maxColor, nbins)
    return np.histogram( img.flatten(), bins)

def floodFill4( img, w, h, x, y, newColor, oldColor):
    ''' img : imagen a modificar
    w,h tamaño de imagen
    x,y origen desde donde llenar
    newColor color a llenar
    oldColor color desde >= que reemplazar
    https://lodev.org/cgtutor/floodfill.html
    '''
    if x >= 0 and x < w and y >= 0 and y < h and img[y,x] >= oldColor and img[y,x] != newColor :
        img[y ,x] = newColor # set color before starting recursion!
        floodFill4(img, w, h, x + 1, y    , newColor, oldColor)
        floodFill4(img, w, h, x - 1, y    , newColor, oldColor)
        floodFill4(img, w, h, x    , y + 1, newColor, oldColor)
        floodFill4(img, w, h, x    , y - 1, newColor, oldColor)

def copyWhite(img):
    H,W = img.shape
    imgW = np.zeros((H,W), dtype=np.uint8)
    for i in range(W):
        for j in range(H):
            if img[j,i] == 255:
                imgW[j,i]=1
    return imgW

def main(args=None):
    print('Hello World!')
    # 1 
    # load image
    img = cv.imread(args.image, cv.IMREAD_GRAYSCALE)
    H,W = img.shape
    print('mostrando imagen base')
    quickShow(img)
    # get centro de polygonos
    p1x, p1y = 49, 19
    p2x, p2y = 40, 40
    p3x, p3y = 100, 30
    # 2
    # definir borde y fondo
    colors, minColor, maxColor = getColors(img)
    getHistogram(img, minColor, maxColor)
    # dos picos aprox en 180 y en 210
    minColor = 120 # borde
    maxColor = 200 # fondo claro
    # 3
    # 3.1
    print('1 PRIMER poligono')
    img1=np.copy(img)
    floodFill4( img1, W, H, p1x, p1y, 255, maxColor)
    img1W = copyWhite(img1)
    print('area del poligono')
    quickShow(img1)
    contour1, hierarchy1 = cv.findContours(img1W, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
    drawing1 = np.zeros((H, W, 3), dtype=np.uint8)
    for i in range(len(contour1)):
        color = (rng.randint(200,256), rng.randint(200,256), rng.randint(200,256))
        cv.drawContours(drawing1, contour1, i, color)
    # Show in a window
    print('contorno del poligono')
    quickShow(drawing1)
    # 3.2
    print('2 SEGUNDO poligono')
    img2=np.copy(img)
    floodFill4( img2, W, H, p2x, p2y, 255, maxColor)
    img2W = copyWhite(img2)
    print('area del poligono')
    quickShow(img2)
    contour2, hierarchy2 = cv.findContours(img2W, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
    drawing2 = np.zeros((H, W, 3), dtype=np.uint8)
    for i in range(len(contour2)):
        color = (rng.randint(200,256), rng.randint(200,256), rng.randint(200,256))
        cv.drawContours(drawing2, contour2, i, color)
    # Show in a window
    print('contorno del poligono')
    quickShow(drawing2)
    # 3.3
    print('3 TERCER poligono')
    img3=np.copy(img)
    floodFill4( img3, W, H, p3x, p3y, 255, maxColor)
    img3W = copyWhite(img3)
    print('area del poligono')
    quickShow(img3)
    contour3, hierarchy3 = cv.findContours(img3W, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
    drawing3 = np.zeros((H, W, 3), dtype=np.uint8)
    for i in range(len(contour3)):
        color = (rng.randint(200,256), rng.randint(200,256), rng.randint(200,256))
        cv.drawContours(drawing3, contour3, i, color)
    # Show in a window
    print('contorno del poligono')
    quickShow(drawing3)
    print('Bye World!')

if __name__ == '__main__':
    sys.exit(main(args=parseArgs(sys.argv[1:])))
