_get poly_

# Requeriments
- python 3.9 with pip & venv
- OpenCV 4.6

# Inputs
- Bordes.png
- Punto interior de cada poligono

# Output
Para cada poligono:
- Imagen de area y contorno 
- Descripcion del contorno

# Idea
1. Cargar imagen en blanco (255) y negro (0)

2. Definir colores: En la imagen de bordes hay 2 colores: fondo y bordes; se hace un histograma hasta ver dos picos lo que indica que 200 es un color de fondo seguro.  

3. Para cada poligono  
	a. Rellenar inundando recursivamente en 4 direcciones. Rellenar con blanco parar cuando es menor a 200.  
	b. Tomar la imagen binaria de fondo negro y relleno blanco  
	c. Encontrar el poligono con cv.findContours

# Install in a virtual environment 
	python3 -m venv ~/pyenv/opencv
	source ~/pyenv/opencv/bin/activate
	pip install opencv-python numpy

# Usage  
	python3 getPoly.py --image aMap.png

# References
- [floodfill](https://lodev.org/cgtutor/floodfill.html)
- [findContours](https://docs.opencv.org/3.4/df/d0d/tutorial_find_contours.html)
