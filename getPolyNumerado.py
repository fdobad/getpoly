import cv2 as cv
import numpy as np
import argparse, sys
import random as rng
rng.seed(12345)

def parseArgs(argv=sys.argv[1:]):
    parser = argparse.ArgumentParser(description='get polygon')
    parser.add_argument('--image','-i', help='imagen para analizar', type=str, default='bordes.png')
    return parser.parse_args(sys.argv[1:])

def quickShow(img):#, z=1):
    '''Show image, press any key to close
    '''
    cv.namedWindow('tmp',cv.WINDOW_FREERATIO)
    #H,W = img.shape
    #reimg = cv.resize(img, (H*z,W*z))
    #cv.imshow('tmp',reimg)
    cv.imshow('tmp',img)
    print('\tresize with mouse, press any key to close...')
    cv.waitKey(0)
    cv.destroyWindow('tmp')

def getColors(img):
    H,W = img.shape
    colors=[]
    for i in range(W):
        for j in range(H):
            if not img[j,i] in colors:
                colors+=[img[j,i]]
    colors.sort()
    return colors, colors[0], colors[-1]

def getHistogram(img, minColor, maxColor, nbins=7):
    bins = np.linspace(minColor, maxColor, nbins)
    return np.histogram( img.flatten(), bins)

def floodFill4( img, w, h, x, y, newColor, oldColor):
    ''' img : imagen a modificar
    w,h tamaño de imagen
    x,y origen desde donde llenar
    newColor color a llenar
    oldColor color desde >= que reemplazar
    https://lodev.org/cgtutor/floodfill.html
    '''
    if x >= 0 and x < w and y >= 0 and y < h and img[y,x] >= oldColor and img[y,x] != newColor :
        img[y ,x] = newColor # set color before starting recursion!
        floodFill4(img, w, h, x + 1, y    , newColor, oldColor)
        floodFill4(img, w, h, x - 1, y    , newColor, oldColor)
        floodFill4(img, w, h, x    , y + 1, newColor, oldColor)
        floodFill4(img, w, h, x    , y - 1, newColor, oldColor)

def copyWhite(img):
    H,W = img.shape
    imgW = np.zeros((H,W), dtype=np.uint8)
    for i in range(W):
        for j in range(H):
            if img[j,i] == 255:
                imgW[j,i]=1
    return imgW

def main(args=None):
    print('Hello World!')
    # 1 
    # load image
    #img = cv.imread(args.image, cv.IMREAD_GRAYSCALE)
    img = cv.imread('38495.png', cv.IMREAD_GRAYSCALE)
    H,W = img.shape
    print('mostrando imagen base')
    quickShow(img)
    # get centro de polygonos
    # Numerados
    pN = [ (90,175), (150,130), (170,105), (200, 90) ]
    # Empty no numerados
    pE = [ (80,140), (100,130), (135,165) ]
    # ver centros
    imgMarked = np.copy(img)
    for p in pN:
        cv.circle(imgMarked, p, 5, (0,0,0), -1)
    for p in pE:
        cv.circle(imgMarked, p, 5, (0,0,0), -1)
    print('ver centros')
    quickShow(imgMarked)
        
    # 2
    # definir borde y fondo
    colors, minColor, maxColor = getColors(img)
    getHistogram(img, minColor, maxColor)
    # dos picos aprox en 180 y en 210
    minColor = 120 # borde
    maxColor = 200 # fondo claro
    # 3

    # para los numerados
    imgN, imgNW = [], []
    contourN, hierarchyN = [], []
    drawingN = []
    for p in pN:
        print('punto ',p)
        imgN += [ np.copy(img) ]
        floodFill4( imgN[-1], W, H, p[0], p[1], 255, maxColor)
        imgNW = [ copyWhite(imgN[-1]) ]
        print('show imgNW')
        quickShow(imgN[-1] )
        contour, hierarchy = cv.findContours( imgNW[-1], cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
        contourN += [contour]
        hierarchyN += [hierarchy]
        drawing = np.zeros((H, W, 3), dtype=np.uint8)
        for i in range(len(contour)):
            color = (rng.randint(200,256), rng.randint(200,256), rng.randint(200,256))
            cv.drawContours(drawing, contour, i, color)
        drawingN += [ drawing ]
        print('show drawing')
        quickShow(drawing)

    # para los no numerados (pN -> pE)
    imgE, imgEW = [], []
    contourE, hierarchyE = [], []
    drawingE = []
    for p in pE:
        print('punto ',p)
        imgE += [ np.copy(img) ]
        floodFill4( imgE[-1], W, H, p[0], p[1], 255, maxColor)
        imgEW = [ copyWhite(imgE[-1]) ]
        print('show imgEW')
        quickShow(imgE[-1] )
        contour, hierarchy = cv.findContours( imgEW[-1], cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
        contourE += [contour]
        hierarchyE += [hierarchy]
        drawing = np.zeros((H, W, 3), dtype=np.uint8)
        for i in range(len(contour)):
            color = (rng.randint(200,256), rng.randint(200,256), rng.randint(200,256))
            cv.drawContours(drawing, contour, i, color)
        drawingE += [ drawing ]
        print('show drawing')
        quickShow(drawing)

    print('Bye World!')

if __name__ == '__main__':
    sys.exit(main(args=parseArgs(sys.argv[1:])))
